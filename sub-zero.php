<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Sub-Zero Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Sub-Zero Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="sub-zero.php"><img srcset="img/logos/sub-zero.png, img/logos/sub-zero@2x.png 2x" alt="Sub-Zero"></a>
                        </picture>
                    </div>
                    <p><a href="http://www.subzero-wolf.com/">www.subzero-wolf.com</a></p>
                    <p><a href="tel:1-800-222-7820">1-800-222-7820</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>
                        <dt>Ice Maker:</dt>
                        <dd>Inside on side wall.</dd>
                        <dt>Refrigerator Drawer:</dt>
                        <dd>Top drawers left side up high.</dd>
                        <dt>Refrigerator:</dt>
                        <dd>Upper refer door hinge.</dd>
                        <dt>Side by Side:</dt>
                        <dd>Freezer door hinge.</dd>
                        <dt>Wine Cooler:</dt>
                        <dd>Upper racks left side.</dd>
                        <dt>700 Series with Drawers:</dt>
                        <dd>Pull top drawer open, in back on side wall.</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Sub-Zero Appliance Service and Repair</h1>
                        
                        <p>Sub Zero appliances are known for their superior performance. Sub Zero refrigerators are designed to keep your lettuce crisp and your frozen items free from freezer burn. You know that your food is well taken care of when you store it in a Sub Zero refrigerator. However, even well-designed appliances need regular maintenance and repairs. That's why we offer Sub Zero refrigerator repair in Seattle, WA and its surrounding communities.</p>
                        <p>We have been proudly servicing Sub Zero refrigerators, freezers, and wine coolers for over 40 years. With our extensive experience and knowledge base we will get your unit back in operating order in a timely and professional manner.</p>

                        <h2>The Appliance Service Station Advantage</h2>

                        <p>We know that your appliances matter to you - without them, many of your daily activities aren't even possible. This is why we strive to provide the highest quality services and repair. Our technicians are not only licensed, bonded, and insured, but they are all factory trained and authorized for warranty services. We will never send a contractor or third-party technician to your home, only our company employees and fully-trained technicians.</p>
                        <p>Our technicians also receive ongoing training on the most recent brand bulletins and the newest technology. They also have phone access to factory assistance if needed. This means they can perform the highest quality work as efficiently as possible.</p>
                        <p>We trust our repairs, and you can too. We offer a full year warranty on all repairs, far past the 30-90 day warranties usually offered on appliance repairs. Not only that, but many parts are available in our trucks - so you don't always have to wait for a part to ship.</p>
                        <p>If you're having a little trouble with your ice maker or just need your condenser cleaned, we can service your Sub Zero refrigerator. You need your Sub Zero refrigerator to run at its full potential, and we can help. </p>

                        <p>Prior to a technician coming out to your home we would like to offer a few tips to possibly help repair the unit without the need for a service call.</p>

                        <ul>
                            <li>If the unit is not cooling properly the condenser may be in need of cleaning. The condenser is what allows the unit to get rid of the heat. On Sub Zero's this is generally located on top of the unit and will resemble a radiator and should be cleaned yearly with the brush attachment on your vacuum cleaner. On wine coolers or drawer units it will be located behind the kick plate and is cleaned in the same manner.</li>
                            <li>If the unit has stopped making ice, here are a couple of things to check prior to scheduling an Appliance Service Station technician to come out to your home. All of the icemakers have a component called a bail arm that allows the unit to detect if more ice is needed or to stop the ice maker from over producing ice. The arm should be in the down position when ice is needed. Also ensure that the water supply to the ice maker is turned on. Locations of the water shutoff valve vary from home to home but, are often located under the kitchen sink or behind the kick plate of the unit. On certain models if it is flashing “service Ice” turn the whole unit off, wait 10 seconds, and turn the unit back on. The word “ice” should always be in the display and not flashing to let you know the icemaker is working.</li>
                            <li>On all other appliances that have an electronic controls, if the unit locks up or shows a fault code of any kind, turn the unit off at the circuit breaker and wait 5 minutes, then restore power to the unit.  Many times this will “reboot” the appliance and no more service is required.</li>
                        </ul>

                        <p>If after attempting these checks and the unit is still not working, or you need any assistance please call us at (206)365-9310 and we will schedule one of our service technicians to come out to your home.  We also offer many other appliance repair service for all your needs in the greater Puget Sound area.  Please see areas we service for a zip code list.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
