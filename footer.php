<footer>
    <div class="awards clearfix tex">
        <div class="container">
            <div class="award col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1 clearfix angies-list">
                <picture>
                    <img srcset="img/angies-list-super-service-2013.png, img/angies-list-super-service-2013@2x.png 2x" alt="Angies List 2013 Super Service Award">
                </picture>
                <p class="margin-0">Appliance Service Station has received Angie’s List Super Service Award for seven years in a row!</p>
            </div>
            <div class="award col-xs-12 col-sm-6 col-lg-5 clearfix bosch">
                <picture>
                    <img srcset="img/bosh-service-partner-2013.png, img/bosh-service-partner-2013@2x.png 2x" alt="2013 Service Partner of the Year - Bosch, Thermador, Gaggenau">
                </picture>
                <p class="margin-0">Appliance Service Station was proud to receive the Bosch 2013 Service Partner of the Year for the North West region.</p>
            </div>
        </div>
    </div>
    <div class="soc">
        <div class="container">
            <div class="col-xs-12 col-sm-4 soc-item">
                <a href="https://www.facebook.com/pages/Appliance-Service-Station/115305881845950" target="_blank"><span class="fb-logo">Facebook Logo</span>Find us on<br>Facebook</a>
            </div>
            <div class="col-xs-12 col-sm-4 soc-item">
                <a href="https://plus.google.com/115643783177177393956/about?hl=en&gl=us" target="_blank"><span class="gp-logo">Google Plus Logo</span>Read &amp; Write Reviews<br>on Google Plus</a>
            </div>
            <div class="col-xs-12 col-sm-4 soc-item">
                <a href="http://www.angieslist.com/companylist/us/wa/seattle/appliance-service-station-inc-reviews-161337.htm" target="_blank"><span class="al-logo">Angies List Logo</span>Read &amp; Write Reviews<br>on Angies List</a>
            </div>
        </div>
    </div>
    <div class="info clearfix tex">
        <div class="stripe-bg">
            <div class="container">
                <div class="col-xs-12 col-md-4 col-md-push-4 vcard">
                    <p class="fn texb">Appliance Service Station</p>
                    <p class="adr">
                        <span class="street-address">12546 Aurora Ave North</span>
                        <span class="city-state-zip">Seattle, WA 98133</span>
                    </p>
                </div>
                <div class="col-xs-12 col-md-4 col-md-pull-4 phone">
                    <ul>
                        <li>Tel: <a href="tel:206-365-9310">1-206-365-9310</a></li>
                        <li>Parts: <a href="tel:866-440-8835">1-866-440-8835</a></li>
                        <li>Fax: 1-206-361-9418</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4 hours">
                    <p class="business-hours">
                        Business Hours:
                        <span class="hours">8am to 5pm</span>
                        <span class="days">Monday thru Friday</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>