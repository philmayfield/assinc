<p>Manufacturers of home appliances authorize warranty service to be performed by reputable service companies. These service companies are contracted by the manufacturer and are held to high standards of performance excellence in servicing their product. This assures the purchaser of the appliance is a satisfied customer from the point-of-sale, to the possible need of adjustment or replacement of a part during the warranty timeline.</p>

<h2>Trained, Professional Technicians</h2>
<p>Our technicians receive regular training from our manufacturers as well as service bulletins. They also have phone access to factory technical assistance when needed.</p>

<h2>Service with Care</h2>
<p>Our technicians take every precaution to make sure your appliance and your home are treated with care and respect.</p>

<h2>Parts</h2>
<p>We use only original factory parts in all warranty repairs, never cheaper "after market" parts.  These parts usually carry a one year manufacturer warranty.  Our trucks are stocked with as many parts as will fit.  Our goal is to complete your repair in just one trip.</p>

<h2>Guarantee</h2>
<p>We guarantee our workmanship for one year from the date of repair.  This is well beyond the industry standard of 30 - 90 days.</p>