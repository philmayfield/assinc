<?php 
require_once('head.php');
$headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Request Service</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <div class="content col-xs-12 col-md-8">
                    <section id="service-form">
                        <h1 class="red-gradient">Request Appliance Service</h1>
                        <p>To request service in the Seattle and Puget Sound area, simply call <a href="tel:206-365-9310">206-365-9310</a> during our regular business hours or if you'd rather simply fill out the form below, and we'll get back to you as soon as possible.</p>
                        [[!FormIt?
                        &hooks=`email,redirect` 
                        &emailTpl=`requestServiceEmailTpl` 
                        &emailTo=`service@assinc.com` 
                        &emailBCC=`philmayfield@gmail.com` 
                        &emailSubject=`Service Request from [[+name]]` 
                        &redirectTo=`9`
                        ]]

                        <form action="" id="[[~[[*id]]]]" method="post">
                            <fieldset class="required">
                                <legend>Required Info</legend>
                                <div class="row">
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="name">Name</label>
                                        <input id="name" name="name" value="[[!+fi.name]]" required="" type="text" />
                                    </div>
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="phone">Phone Number</label>
                                        <input id="phone" name="phone" maxlength="20" value="[[!+fi.phone]]" required="" type="tel" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="make">Appliance Make</label>
                                        <select name="make" id="make" required="">
                                            <option value="" selected="selected"></option>
                                            <option value="Asko">Asko</option>
                                            <option value="Bertazonni">Bertazonni</option>
                                            <option value="BlueStar">BlueStar</option>
                                            <option value="Bosch">Bosch</option>
                                            <option value="Dacor">Dacor</option>
                                            <option value="DCS">DCS</option>
                                            <option value="FisherPaykel">Fisher &amp; Paykel</option>
                                            <option value="Gaggenau">Gaggenau</option>
                                            <option value="Miele">Miele</option>
                                            <option value="SubZero">Sub Zero</option>
                                            <option value="Thermador">Thermador</option>
                                            <option value="ULine">U-Line</option>
                                            <option value="Viking">Viking</option>
                                            <option value="Wolf">Wolf</option>
                                            <option value="Zephyr">Zephyr</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="type">Appliance Type</label>
                                        <select name="type" id="type" required="">
                                            <option value="" selected="selected"></option>
                                            <optgroup label="Kitchen">
                                                <option value="refrigerator">Refrigerator</option>
                                                <option value="dishwasher">Dishwasher</option>
                                                <option value="range">Range</option>
                                                <option value="oven">Oven</option>
                                                <option value="cooktop">Cooktop</option>
                                                <option value="microwave">Microwave</option>
                                                <option value="compactor">Compactor</option>
                                                <option value="hood">Hood</option>
                                                <option value="kitchen-other">Other Kitchen</option>
                                            </optgroup>
                                            <optgroup label="Laundry">
                                                <option value="washer">Washer</option>
                                                <option value="dryer">Dryer</option>
                                                <option value="other-laundry">Other Laundry</option>
                                            </optgroup>
                                            <optgroup label="Outdoor">
                                                <option value="bbq">Barbeque</option>
                                                <option value="other-outdoor">Other Outdoor</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-field col-xs-12 col-md-12">
                                        <label for="message">What can we help you with?</label>
                                        <textarea name="message" id="message" cols="30" rows="5" value="[[!+fi.message]]" required=""></textarea>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Optional Info</legend>
                                <p>The information below is optional, but will greatly improve the accuracy of our response.</p>
                                <div class="row">
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="model">Appliance Model</label>
                                        <input id="model" name="model" value="[[!+fi.model]]" type="text" />
                                    </div>
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="serial">Serial Number</label>
                                        <input id="serial" name="serial" value="[[!+fi.serial]]" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="purchase-date">Aproximate Purchase Date</label>
                                        <input id="purchase-date" name="purchase-date" value="[[!+fi.purchase-date]]" type="text" />
                                    </div>
                                    <div class="form-field col-xs-12 col-md-6">
                                        <label for="email">Email Address</label>
                                        <input id="email" name="email" value="[[!+fi.email]]" type="email" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-field col-xs-12 col-md-12">
                                        <label for="address">Street Address</label>
                                        <input type="text" id="address" name="address" value="[[!+fi.address]]">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-field col-xs-12 col-md-5">
                                        <label for="city">City</label>
                                        <input type="text" id="city" name="city" list="cities" value="[[!+fi.city]]">
                                        <datalist id="cities">
                                            <option value="Auburn">
                                            <option value="Bellevue">
                                            <option value="Bothell">
                                            <option value="Mill Creek">
                                            <option value="Edmonds">
                                            <option value="Issaquah">
                                            <option value="Kenmore">
                                            <option value="Kent">
                                            <option value="Kirkland">
                                            <option value="Lynnwood">
                                            <option value="Maple Valley">
                                            <option value="Medina">
                                            <option value="Mercer Island">
                                            <option value="Mountlake Terrace">
                                            <option value="Redmond">
                                            <option value="Redondo">
                                            <option value="Renton">
                                            <option value="Woodinville">
                                            <option value="Sammamish">
                                            <option value="Seattle">
                                            <option value="Everett">
                                            <option value="Mukiteo">
                                        </datalist>
                                    </div>
                                    <div class="form-field col-xs-12 col-md-4">
                                        <label for="state">State</label>
                                        <select name="state" id="state">
                                            <option value="WA" selected>Washington</option>
                                            <option value="" disabled="disabled">----------</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                    </div>
                                    <div class="form-field col-xs-12 col-md-3">
                                        <label for="zip">Zip</label>
                                        <input type="text" id="zip" name="zip" value="[[!+fi.zip]]">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="col-xs-12 col-md-11 align-center">
                                <input id="service-form-button" class="button submit tex" value="Send" type="submit" />
                                <img src="img/ajax-loader.gif" alt="loading..." class="ajax-loader" style="display:none;">
                            </div>
                        </form>
                    </section>
                </div>
                <aside class="col-xs-12 col-md-4">
                    <section>
                        <h2 class="texb align-center">Service Area</h2>
                        <p>Appliance Service Station services most of Seattle, South King County, Eastside, Tacoma and Pierce County, and Snohomish County to Everett.</p>
                        <p>To verify that we service your specific area, simply type in the 5 digit zip code of the appliance you wish to be serviced in the box below.</p>
                        <form action="#" id="zip-check" class="auto">
                            <label for="zc-zip">Zip Code</label>
                            <input type="number" maxlength="5" min="11111" max="99999" id="zc-zip" size="6">
                            <input type="submit" class="button inline-btn tex" value="Check">
                        </form>
                        <p id="zc-msg" class="texb hidden"></p>
                    </section>
                </aside>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
