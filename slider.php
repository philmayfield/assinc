<section id="slider" class="slider <?php if(isset($headerSize) && $headerSize == 's'){echo ' visuallyhidden';} ?>">
    <div class="slide refrigerator slide-left tex off visuallyhidden">
        <h3>Refrigerator Repair</h3>
        <ul>
            <li class="first texb">Servicing:</li>
            <li>Bosch</li>
            <li>Sub Zero</li>
            <li>Dacor</li>
            <li>Fisher &amp; Paykel</li>
            <li>U-line</li>
            <li>Viking</li>
        </ul>
        <span class="slider-img"></span>
    </div>
    <div class="slide range slide-right tex off visuallyhidden">
        <h3>Range &amp; Oven Repair</h3>
        <ul>
            <li class="first texb">Servicing:</li>
            <li>Bertazzoni</li>
            <li>BlueStar</li>
            <li>Bosch</li>
            <li>Dacor</li>
            <li>DCS</li>
        </ul>
        <ul>
            <li>Fisher &amp; Paykel</li>
            <li>Miele</li>
            <li>Thermador</li>
            <li>Viking</li>
            <li>Wolf</li>
        </ul>
        <span class="slider-img"></span>
    </div>
    <div class="slide dishwasher slide-left tex off visuallyhidden">
        <h3>Dishwasher Repair</h3>
        <ul>
            <li class="first texb">Servicing:</li>
            <li>Bosch</li>
            <li>Sub Zero</li>
            <li>Dacor</li>
            <li>Fisher &amp; Paykel</li>
            <li>U-line</li>
            <li>Viking</li>
        </ul>
        <span class="slider-img"></span>
    </div>
    <div class="slide washer slide-right tex off visuallyhidden">
        <h3>Washer &amp; Dryer Repair</h3>
        <ul>
            <li class="first texb">Servicing:</li>
            <li>Bosch</li>
            <li>Dacor</li>
            <li>DCS</li>
            <li>Fisher &amp; Paykel</li>
            <li>Thermador</li>
            <li>Viking</li>
            <li>Wolf</li>
        </ul>
        <span class="slider-img"></span>
    </div>
</section>