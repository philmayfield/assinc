<?php 
require_once('head.php');
$headerSize = 's';
?>
    <title>Appliance Service Station Inc. - High End Appliance Repair</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <div class="content col-xs-12 col-md-5 col-md-push-7">
                    <section>
                        <h1 class="col-md-hide">About Appliance Service Station Inc.</h1>
                        <p class="align-center section-top-spacing">12546 Aurora Ave. North, <span class="nowrap">Seattle WA, 98133</span></p>
                        <iframe class="gmap" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2684.1264318399535!2d-122.34456720000004!3d47.7207952!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54901135e42fac61%3A0x2bb19fbbee7030c7!2s12546+Aurora+Ave+N%2C+Seattle%2C+WA+98133!5e0!3m2!1sen!2sus!4v1419400680112" width="100%" height="400" frameborder="0"></iframe>
                    </section>
                    <section>
                        <h2>Contact Numbers</h2>
                        <dl>
                            <dt>Seattle</dt>
                            <dd>206-365-9310</dd>
                            <dt>Bellevue / Eastside</dt>
                            <dd>425-455-3749</dd>
                            <dt>Snohomish County</dt>
                            <dd>425-337-2148</dd>
                            <dt>South King County</dt>
                            <dd>206-824-3180</dd>
                            <dt>Pierce County / Tacoma</dt>
                            <dd>866-797-0670</dd>
                            <dt>Toll Free Direct Parts Line</dt>
                            <dd>866-440-8835</dd>
                            <dt>Fax Line</dt>
                            <dd>206-361-9418</dd>
                        </dl>
                    </section>
                    <section>
                        <h2>We are hiring!</h2>
                        <p>Appliance Service Station is now interviewing for technician positions. We are not currently accepting applications for trainees. To qualify you must already posses a Washington State Specialty Electricians License – Class 07D.  If you qualify feel free to send us your resume or fill out the application at the link below. You may fax your application to 206-361-9418.</p>
                        <p><strong><a href="">Technician Application</a></strong> (pdf)<br> - <a href="http://get.adobe.com/reader/" target="_blank">Download Adobe Acrobat</a></p>
                    </section>
                </div>
                <aside class="content col-xs-12 col-md-7 col-md-pull-5">
                    <section>
                        <h2 class="col-xs-hide col-md-show likeh1 red-gradient">About Appliance Service Station Inc.</h2>
                        <p>Appliance Service Station is locally-owned, and has been faithfully serving the Puget Sound area since 1947.</p>
                        <h3>Our Guarantee</h3>
                        <p>We guarantee our workmanship for one year from the date of repair.  This is well beyond the industry standard of 30 - 90 days.</p>
                        <h3>Factory Authorized</h3>
                        <p>We are Factory Authorized service for the brands we represent.</p>
                        <h3>Trained, Professional Technicians</h3>
                        <p>Our technicians receive regular training from our manufacturers as well as service bulletins and they have phone access to factory technical assistance when needed.</p>
                        <h3>Service With Care</h3>
                        <p>Our technicians take every precaution to make sure your appliance and your home are treated with care and respect.</p>
                        <h3>Employees</h3>
                        <p>All persons with Appliance Service Station are actual company employees, not private contractors or temporary help.</p>
                        <h3>Parts</h3>
                        <p>We use only original factory parts in all warranty repairs, never cheaper "after market" parts.  These parts usually carry a one year manufacturer warranty.  Our trucks are stocked with as many parts as will fit.  Our goal is to complete your repair in just one trip.</p>
                    </section>
                    <section>
                        <h2>The Mission and Purpose of Appliance Service Station:</h2>
                        <ul class="list-spacing">
                            <li>Conduct business transactions in an ethical, fair and honest manner with customers, employees and vendors.</li>
                            <li>Manage the company in a way that recognizes responsibility to our employees, our customers, the company itself and the community.</li>
                            <li>Provide quality technical service, along with the very best customer service available anywhere.</li>
                            <li>Our goal is to be the undisputed highest quality service company in Washington State.</li>
                            <li>Further our reputation of delivering what we promise.</li>
                            <li>Plan ahead to meet anticipated future needs of our customers and the future demands the market will make on our company.</li>
                            <li>Operate the company in a way that generates a fair and acceptable level of profit.</li>
                            <li>Keep abreast with current technology and to use it, where possible, to improve and enhance the quality of the service we provide while maintaining a profitable financial position.</li>
                            <li>Provide a working environment that encourages quality work, creativity, organization, good personal relationships while minimizing the stress that every work day brings.</li>
                            <li>Maintain policies and procedures that strengthens the company's position as an equal opportunity employer as well as maintaining compliance with all federal, state, and local laws.</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Privacy Policy Notice</h2>
                        <p>This privacy notice discloses the privacy practices for Appliance Service Station Inc., and our web site applianceservicestation.com and assinc.com.  This privacy notice applies solely to information collected by this web site and through our customer service activities over the phone. It will notify you of the following:</p>
                        <ol class="list-spacing">
                            <li>What personally identifiable information is collected from you, how it is used, and with whom it may be shared.</li>
                            <li>What choices are available to you regarding the use of your data.</li>
                            <li>The security procedures in place to protect the misuse of your information.</li>
                            <li>How you can correct any inaccuracies in the information.</li>
                        </ol>
                        <h3>Information Collection, Use, and Sharing</h3>
                        <p>We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email, fax or other direct contact from you. We will not sell or rent this information to anyone.</p>
                        <p>We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order. We are obligated to provide information to the manufacturer of your appliance if we provide warranty service.  This is done via a warranty service claim for payment.</p>
                        <p>Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>
                        <h3>Your Access to and Control Over Information</h3>
                        <p>You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p>
                        <ul class="list-spacing">
                            <li>See what data we have about you, if any.</li>
                            <li>Change/correct any data we have about you.</li>
                            <li>Express any concern you have about our use of your data.</li>
                        </ul>
                        <h3>Security</h3>
                        <p>While we use encryption to protect sensitive information transmitted online, such as when we submit your credit card number for payment, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment. As a matter of policy we do not store your credit card information in our systems.</p>
                    </section>
                </aside>
                
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
