<h2 class="texb">Brands We Service</h2>
<ul class="logo-list">
    <li>
        <picture>
            <a href="bosch.php"><img class="responsive" srcset="img/logos/bosch.png, img/logos/bosch@2x.png 2x" alt="Bosch"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="sub-zero.php"><img class="responsive" srcset="img/logos/sub-zero.png, img/logos/sub-zero@2x.png 2x" alt="Sub-Zero"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="wolf.php"><img class="responsive" srcset="img/logos/wolf.png, img/logos/wolf@2x.png 2x" alt="Wolf"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="dacor.php"><img class="responsive" srcset="img/logos/dacor.png, img/logos/dacor@2x.png 2x" alt="Dacor"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="viking.php"><img class="responsive" srcset="img/logos/viking.png, img/logos/viking@2x.png 2x" alt="Viking"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="asko.php"><img class="responsive" srcset="img/logos/asko.png, img/logos/asko@2x.png 2x" alt="Asko"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="dcs.php"><img class="responsive" srcset="img/logos/dcs.png, img/logos/dcs@2x.png 2x" alt="DCS by Fisher&amp;Paykel"></a>
        </picture>
    </li>
</ul>
<ul class="logo-list">
    <li>
        <picture>
            <a href="fisher-paykel.php"><img class="responsive" srcset="img/logos/fisher-paykel.png, img/logos/fisher-paykel@2x.png 2x" alt="Fisher&amp;Paykel appliances"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="gaggenau.php"><img class="responsive" srcset="img/logos/gaggenau.png, img/logos/gaggenau@2x.png 2x" alt="Gaggenau"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="miele.php"><img class="responsive" srcset="img/logos/miele.png, img/logos/miele@2x.png 2x" alt="Miele"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="thermador.php"><img class="responsive" srcset="img/logos/thermador.png, img/logos/thermador@2x.png 2x" alt="Thermador"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="u-line.php"><img class="responsive" srcset="img/logos/u-line.png, img/logos/u-line@2x.png 2x" alt="U-Line"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="zephyr.php"><img class="responsive" srcset="img/logos/zephyr.png, img/logos/zephyr@2x.png 2x" alt="Zephyr"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="bertazzoni.php"><img class="responsive" srcset="img/logos/bertazzoni.png, img/logos/bertazzoni@2x.png 2x" alt="Bertazzoni"></a>
        </picture>
    </li>
    <li>
        <picture>
            <a href="bluestar.php"><img class="responsive" srcset="img/logos/bluestar.png, img/logos/bluestar@2x.png 2x" alt="BlueStar"></a>
        </picture>
    </li>
</ul>