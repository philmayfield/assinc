<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Dacor Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Dacor Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="dacor.php"><img srcset="img/logos/dacor.png, img/logos/dacor@2x.png 2x" alt="Dacor"></a>
                        </picture>
                    </div>
                    <p class="approved"><span class="icon">Checkmark</span> Appliance Service Station is a Dacor certified repair provider.</p>
                    <p><a href="http://www.dacor.com/">www.dacor.com</a></p>
                    <p><a href="tel:1-800-793-0093">1-800-793-0093</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>
                        <dt>BBQ:</dt>
                        <dd>Back of the grille.</dd>
                        <dt>Coffee Maker:</dt>
                        <dd>With front panel and brewing unit open door by piston assembly.</dd>
                        <dt>Cooktop:</dt>
                        <dd>Underneath bottom of unit.</dd>
                        <dt>Downdraft:</dt>
                        <dd>Lower half of unit front side, half way down, under cabinet.</dd>
                        <dt>Dishwasher:</dt>
                        <dd>Around side of the door.</dd>
                        <dt>Hood:</dt>
                        <dd>Behind the filters.</dd>
                        <dt>Oven:</dt>
                        <dd>Behind grille area above opening to over where door lock is on either right or left side.</dd>
                        <dt>Oven with 20-color display:</dt>
                        <dd>Open door left side lower corner (flip up tag).</dd>
                        <dt>Range:</dt>
                        <dd>Behind grille area above opening to over where door lock is on either right or left side, or inside bottom pan storage on left.</dd>
                        <dt>Refrigerator:</dt>
                        <dd>Inside top right.</dd>
                        <dt>Warming Drawer:</dt>
                        <dd>On the side of the drawer or under drawer.</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Dacor Appliance Service and Repair</h1>
                        <p>Appliance Service Station has been an authorized servicer for Dacor appliances for over 25 years since Dacor appliances became available.  All our technicians are factory trained and certified.  Dacor manufactures ranges, cooktops, wall ovens, dishwashers, warming drawers, microwaves, BBQ’s, refrigerators, wine dispensers, and ventilation products.</p>

                        <p>Prior to a technician coming out to your home we would like to offer a few tips to possibly help repair the unit without the need for a service call.</p>

                        <ul>
                            <li>If your dishwasher is not draining, remove the filter in the bottom of unit and ensure the filter is clean, if your dishwasher is draining to an air gap, remove the cap on the air gap and inspect it for clogs.</li>
                            <li>If the dishwasher is not filling with water, check to make sure the water supply valve (usually under the kitchen sink) is turned on.</li>
                            <li>If your dishwasher is not cleaning as well as it used to.  Check the spray arms for clogs.</li>
                            <li>If the gas burners are not lighting properly or clicking after lit.  Your igniters may need cleaning, this can usually be done by cleaning the tip of the igniter with a fine file, or an emery board.  Follow that up with cleaning the igniter with a Q-tip dipped in rubbing alcohol.</li>
                            <li>On barbeques the burner tubes can become clogged with food debris and will require occasional cleaning.   This can be accomplished by using a small piece of wire or paperclip and going around to all the holes in the burner and cleaning them out.</li>
                            <li>On all other appliances that have an electronic controls, if the unit locks up or shows a fault code of any kind, turn the unit off at the circuit breaker and wait 5 minutes, then restore power to the unit.  Many times this will “reboot” the appliance and no more service is required.</li>
                        </ul>

                        <p>If after attempting these checks and the unit is still not working, or you need any assistance please call us at (206)365-9310 and we will schedule one of our service technicians to come out to your home.  We also offer many other appliance repair service for all your needs in the greater Puget Sound area.  Please see areas we service for a zip code list.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
