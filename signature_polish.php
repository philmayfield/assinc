<?php 
require_once('head.php');
$headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Signature Appliance Polish</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <div class="content col-xs-12 col-md-8">
                    <section>
                        <h1 class="red-gradient">Signature Professional Polish</h1>
                        <p>We are frequently asked what we recommend for polishing and conditioning stainless steel appliances. We have found Signature Professional Polish to be an excellent product.</p>

                        <p>You can purchase 16 oz bottles directly from us and have it shipped to your home anywhere in the continental US.  All of our technicians also carry it in their trucks and can be added to your service call invoice.</p>

                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <h2>Order Signature Polish</h2>

                                <div class="row">
                                    <div class="signature-polish-item col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                                        <span class="name osb">Signature Polish</span>
                                        <span class="price osb float-right">$22.95</span>
                                        <br>
                                        <span class="details">1 bottle - 16oz</span>
                                        <span class="details float-right">+ shipping</span>
                                    </div>
                                </div>
                                
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="signature-polish-form">
                                    <input type="hidden" name="cmd" value="_s-xclick">
                                    <input type="hidden" name="hosted_button_id" value="D6PDGSY6QME9W">
                                    <input class="hidden" type="image" src="http://assinc.com/modx/img/1px-trans.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                    <input type="submit" value="Buy Now" class="button">
                                </form>
                                
                                <div class="secured-by-paypal align-center">
                                    <a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" target="_blank">
                                        <img src="https://www.paypalobjects.com/webstatic/mktg/logo/bdg_secured_by_pp_2line.png" alt="Secured by PayPal">
                                    </a>
                                    <br>
                                    <a href="https://www.paypal.com/webapps/mpp/how-paypal-works" target="_blank">How PayPal Works</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4 align-center">
                                <img class="responsive" srcset="img/signature-polish@2x.png" alt="Signature Professional Polish">
                                <h3 class="align-center margin-top-20 margin-bottom-10">Recommended by:</h3>
                        <div class="col-xs-12 padding-0 align-center recommended">
                            <a href="sub-zero.php"><img srcset="img/logos/sub-zero.png, img/logos/sub-zero@2x.png 2x" alt="Sub-Zero"></a>
                            <a href="wolf.php"><img srcset="img/logos/wolf.png, img/logos/wolf@2x.png 2x" alt="Wolf"></a>
                        </div>
                            </div>
                        </div>
                        
                        
                    </section>
                </div>
                <aside class="col-xs-12 col-md-4">
                    <h2>Multi-Surface</h2>
                    <p>Signature Polish removes grease, dirt, fingerprints, smudges &amp; water marks from stainless steel, including all brands of stainless steel kitchen appliances, aluminum, chrome, leather &amp; vinyl as well as many other surfaces including cars, boats, motor homes &amp; motorcycles.</p>

                    <h2>Superior Wood Polish</h2>
                    <p>Signature Polish is considered to be one of the best polish's for wood, including painted or wood cabinets, antiques, fine furniture, all wood and laminate surfaces.</p>

                    <h2>Finest Ingredients</h2>
                    <p>Signature Polish is made of the finest ingredients for cleaning &amp; protecting many different indoor &amp; outdoor surfaces in your home or business.</p>

                    <h2>Environmentally Friendly</h2>
                    <p>Formulated with NO harmful vapors, toxic propellants or offensive smells. Signature Polish is an enviromentally friendly &amp; safe household cleaner for your family.</p>

                    <h2>Protective Barrier</h2>
                    <p>Signature Polish protects by forming a surface barrier against dirt without leaving a build up.</p>
                </aside>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
