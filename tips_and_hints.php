<?php 
require_once('head.php');
$headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Tips and Hints</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <div class="content col-xs-12 col-md-10">
                    <section>
                        <h1 class="red-gradient">Appliance Tips &amp; Hints</h1>
                        
                        <p>One of the most common questions we are asked is whether an appliance should be repaired or replaced.</p>
                        
                        <p>Here are some questions you need to ask yourself to help you make a decision:</p>
                        
                        <ul class="list-spacing">
                            <li>Is the appliance in good mechanical condition, other than the current problem?</li>
                            <li>Is the appliance in good cosmetic condition or are there dents and scratches?</li>
                            <li>Are there strange odors, evidence of rust, any funny noises or leaks?</li>
                            <li>Do you have an emotional attachment to the appliance?</li>
                            <li>What is the normal useful life of the appliance and how old is it?</li>
                        </ul>
                        
                        <p>Although there are exceptions, we generally do not recommend that you invest more than half of the replacement cost in repairing an appliance. For that reason it is a good idea to scout out the replacement cost before getting too far into the repair. Here are some things to think about when considering the total cost of replacement.</p>
                        
                        <ul class="list-spacing">
                            <li>The cost (plus tax) of the new appliance.</li>
                            <li>Any fees for getting a firm estimate on the repair of the old appliance.</li>
                            <li>The cost of disposing the old appliance.</li>
                            <li>The cost of installing the new appliance.</li>
                            <li>On many built-in products such as refrigerators or dishwashers there are often custom door panels involved. Will your old panels fit the new unit?</li>
                            <li>Can you find a new unit that will fit in the current installation without necessary modifications to the cabinetry?</li>
                        </ul>
                        
                        <p>If you can extend the useful life of your appliance another two years or more it is often actually more economical to repair the old unit than replace it.  If you are just not sure,  you could invest in the cost of getting a solid estimate of the repair and let us help you make that decision with all the facts.  The last thing we want you to do is have us repair a unit that should not be repaired.  On the other hand we hate to see you spend twice the money on a new appliance if it is not necessary.</p>
                    </section>
                    
                    <section>
                        <h2>Dishwashers</h2>

                        <p>Reasons for dishwashers not getting things clean are:</p>

                        <ul class="list-spacing">
                            <li>Water not hot enough - it should be at least 140 degrees.</li>
                            <li>Old detergent that is not melting - if you don't use the machine a lot buy smaller boxes.</li>
                            <li>Not properly draining due to a plugged drain or plugged air gap.</li>
                            <li>Large objects obstructing the wash arms so they cannot rotate properly.</li>
                            <li>Too much soap. Over time the excess soap can build up and cause trouble. Run the machine empty and check mid cycle to see if there is evidence of soap in the water. If so repeat until the excess is gone.</li>
                        </ul>
                    </section>
                    
                    <section>
                        <h2>Refrigerators</h2>

                        <p>The most common cause of refrigeration problems:</p>

                        <ul class="list-spacing">
                            <li>Failure to clean the condenser. This component must be able to vent heat into the ambient air around the refrigerator or freezer. If it is clogged with house dust or animal fur for example, it will reduce the efficiency of the unit at best, and worse, cause components to overheat and perhaps be damaged beyond repair. Your owner’s manual probably recommends cleaning the condenser on a regular basis. Click HERE for information on our refrigerator condenser cleaning special.</li>
                            <li>Is the door closing properly? If not, it can cause the interior light to stay on and that will affect cooling. A defective door switch can do the same thing. Here is a way to test the door seal. Open the door and place a dollar bill between the seal and the body of the box, then close the door. Is the dollar bill easy to pull out or does it fall out on its own? If so you may have a sealing problem. Another sure sign of this is frost around the area where the door seals. That indicates warm air is getting into the box and when it mixes with the cold air and forms frost. You can also make sure nothing in the box is obstructing it from closing completely.</li>
                        </ul>
                    </section>

                    <section>
                        <h2>Laundry Products</h2>
                        
                        <h3>Dryers:</h3>
                        
                        <p>If the dryer taking too long to dry:</p>
                            
                        <ul class="list-spacing">
                            <li>Make sure the clothes coming out of the washer are properly spun dry. If they are too wet it will take the dryer a very long time to get them dry.</li>
                            <li>Check to be sure that the dryer has not been pushed back and is not pinching the vent. We sometimes find animal nests in the duct work or in the vent on the outside of the house. Anything that obstructs the vent will cause problems.</li>
                            <li>Check to be sure that the dryer has not been pushed back and is not pinching the vent. We sometimes find animal nests in the duct work or in the vent on the outside of the house. Anything that obstructs the vent will cause problems.</li>
                            <li>Be sure you are cleaning the lint screen every time you put in a new load. This will also save some on your fuel bill. Note that some fabric softener products also clog the lint screen much faster and some manufacturers actually recommend that they not be used. They can also affect the sensors that tell some models of dryer if the cloths are dry yet or not.</li>
                        </ul>

                        <h3>Washers:</h3>

                        <p>Front loading use much less soap then top loaders.  Make sure you are using the type and amount of soap recommended by your manufacturer.  Oversudsing and detergent build up in the clothes can result.  Do not overload these machines either as they depend on the tumbling action for part of the cleaning process and packed in clothes don't tumble well.</p>
                    </section>

                    <section>
                        <h2>Cooking Products</h2>

                        <h3>Ovens:</h3>

                        <p>Today's electronic and ranges use cooling fans to protect the sensitive components on the control circuit boards.  It is common for these fans to not come on immediately and also to stay on after the appliance has been turned off.  They will normally turn themselves off once the ovens internal temperatures reach a safe level.  Also see the power down hint under microwaves below.</p>

                        <h3>Gas Cooktops:</h3>

                        <p>Most modern gas cooktops use electronic ignition to light the gas.  This ignition system provides a spark until the flame appears then automatically shuts off.  If the sparking continues after the flame is burning it may be because of a burner cap out of position or dirt on the cap or ring.  Try cleaning all the burners and making sure everything is seated properly.</p>

                        <h3>Electric Cooktops:</h3>

                        <p>If one of the surface is not heating and it is the type you can unplug for cleaning, make sure it is properly seated in the plug.  You can check to see if the plug itself is the problem by moving elements around.  If the problem stays in the same place there is most likely a wiring problem that will require a technician but if the problem moves with the element then it is probably bad.</p>

                        <h3>Microwaves:</h3>

                        <p>If your oven (or other electronic controlled appliance) has digital controls and the numbers start flashing and the until will not program, try unplugging the unit (or turning off the circuit breaker if it is wired in) and letting it sit for about a minute, then plug it back in.  Sometimes that's all it takes to reset the processors and get everything working correctly</p>
                    </section>
    
                </div>
                <aside class="col-xs-12 col-sm-10 col-sm-push-1 col-md-2 col-md-push-0 align-center no-left-pad">
                    <?php include_once('brands.php'); ?>
                </aside>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
