<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Brands we Service</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <div class="content col-xs-12 col-md-6">
                    <section>
                        <h1 class="red-gradient">Brands we service</h1>
                        <p>Appliance Service Station is here to help. With over 65 years of experience servicing a variety of appliances, we’re here to help your household run smoothly again.</p>
                        <ul class="brand-list tex">
                            <li><a href="asko.php">Asko</a></li>
                            <li><a href="bertazonni.php">Bertazonni</a></li>
                            <li><a href="bluestar.php">BlueStar</a></li>
                            <li><a href="bosch.php">Bosch</a></li>
                            <li><a href="dacor.php">Dacor</a></li>
                            <li><a href="dcs.php">DCS</a></li>
                            <li><a href="fisher-paykel.php">Fisher &amp; Paykel</a></li>
                            <li><a href="gaggenau.php">Gaggenau</a></li>
                            <li><a href="miele.php">Miele</a></li>
                            <li><a href="sub-zero.php">Sub Zero</a></li>
                            <li><a href="thermador.php">Thermador</a></li>
                            <li><a href="u-line.php">U-Line</a></li>
                            <li><a href="viking.php">Viking</a></li>
                            <li><a href="wolf.php">Wolf</a></li>
                            <li><a href="zephyr.php">Zephyr</a></li>
                        </ul>
                    </section>
                </div>
                <aside class="col-xs-12 col-md-6">

                    <?php include_once('aside-info.php') ?>

                </aside>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
