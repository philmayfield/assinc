<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Bosch Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Bosch Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="bosch.php"><img srcset="img/logos/bosch.png, img/logos/bosch@2x.png 2x" alt="Bosch"></a>
                        </picture>
                    </div>
                    <p class="approved"><span class="icon">Checkmark</span> Appliance Service Station is a Bosch certified repair provider.</p>
                    <p><a href="http://www.boschappliances.com">www.boschappliances.com</a></p>
                    <p><a href="tel:1-800-944-2904">1-800-944-2904</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>
                        <dt>Compactor:</dt>
                        <dd>Around the door cabinet on the right side.</dd>
                        <dt>Cooktops:</dt>
                        <dd>Underneath the bottom of the unit in the middle</dd>
                        <dt>Dishwasher:</dt>
                        <dd>Around the side of the door, or on the side of the unit, or between the hinge and body.</dd>
                        <dt>Hood:</dt>
                        <dd>Behind the filters.</dd>
                        <dt>Microwave:</dt>
                        <dd>Inside the door, on the cabinet side.</dd>
                        <dt>Oven:</dt>
                        <dd>Around the cabinet on the right side.</dd>
                        <dt>Range:</dt>
                        <dd>Around the cabinet on the right side.</dd>
                        <dt>Washer:</dt>
                        <dd>Around the door (must open the door to see).</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Bosch Appliance Service and Repair</h1>
                        <p>We have been servicing Bosch home appliances since their introduction to the American market.  Appliance Service Station was also the proud recipient of the Service Partner of the Year Award for the Northwest region in 2013.  Bosch manufactures, dishwashers, cooktops, wall ovens, ranges, microwaves, ventilation products, coffee makers, washers and dryers.  Our technicians are factory trained on all these appliances.</p>

                        <p>Prior to a technician coming out to your home we would like to offer a few tips to possibly help repair the unit without the need for a service call.</p>

                        <ul>
                            <li>If your dishwasher is not draining, remove the filter in the bottom of unit and ensure the filter is clean, if your dishwasher is draining to an air gap, remove the cap on the air gap and inspect it for clogs.</li>
                            <li>If the dishwasher is not filling with water, check to make sure the water supply valve (usually under the kitchen sink) is turned on.</li>
                            <li>If your dishwasher is not cleaning as well as it used to.  Check the spray arms for clogs.</li>
                            <li>If the dryer is taking longer to dry, check your lint screen and the vent to the outside.</li>
                            <li>On all other appliances that have an electronic controls, if the unit locks up or shows a fault code of any kind, turn the unit off at the circuit breaker and wait 5 minutes, then restore power to the unit.  Many times this will “reboot” the appliance and no more service is required.</li>
                        </ul>

                        <p>If after attempting these checks and the unit is still not working, or you need any assistance please call us at (206)365-9310 and we will schedule one of our service technicians to come out to your home.  We also offer many other appliance repair service for all your needs in the greater Puget Sound area.  Please see areas we service for a zip code list.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
