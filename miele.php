<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Miele Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Miele Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="miele.php"><img srcset="img/logos/miele.png, img/logos/miele@2x.png 2x" alt="Miele"></a>
                        </picture>
                    </div>
                    <p class="approved"><span class="icon">Checkmark</span> Appliance Service Station is a Miele certified repair provider.</p>
                    <p><a href="http://www.mieleusa.com/">www.mieleusa.com</a></p>
                    <p><a href="tel:1-800-463-0260">1-800-463-0260</a></p>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Miele Appliance Service and Repair</h1>
                        <p>We have been proud to be an authorized service provider for Miele since 2012.  Miele manufactures ranges, ovens, warming drawers, cooktops, ventilation hoods, coffee machines, refrigerators, dishwashers, washers and dryers.  Our technicians are factory trained on all these appliances.</p>

                        <p>Prior to a technician coming out to your home we would like to offer a few tips to possibly help repair the unit without the need for a service call.</p>

                        <ul>
                            <li>If your dishwasher is not draining, remove the filter in the bottom of unit and ensure the filter is clean, if your dishwasher is draining to an air gap, remove the cap on the air gap and inspect it for clogs.</li>
                            <li>If the dishwasher is not filling with water, check to make sure the water supply valve (usually under the kitchen sink) is turned on.</li>
                            <li>If your dishwasher is not cleaning as well as it used to.  Check the spray arms for clogs.</li>
                            <li>If the dryer is taking longer to dry, check your lint screen and the vent to the outside.</li>
                            <li>On all other appliances that have an electronic controls, if the unit locks up or shows a fault code of any kind, turn the unit off at the circuit breaker and wait 5 minutes, then restore power to the unit.  Many times this will “reboot” the appliance and no more service is required.</li>
                        </ul>

                        <p>If after attempting these checks and the unit is still not working, or you need any assistance please call us at (206)365-9310 and we will schedule one of our service technicians to come out to your home.  We also offer many other appliance repair service for all your needs in the greater Puget Sound area.  Please see areas we service for a zip code list.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
