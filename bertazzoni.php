<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Bertazzoni Appliance Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Bertazzoni Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="bertazzoni.php"><img srcset="img/logos/bertazzoni.png, img/logos/bertazzoni@2x.png 2x" alt="Bertazzoni"></a>
                        </picture>
                    </div>
                    <p><a href="http://us.bertazzoni.com/">us.bertazzoni.com/</a></p>
                    <p><a href="tel:1-866-305-0010">1-866-305-0010</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>
                        <dt>Microwave:</dt>
                        <dd>Unside door on the bottom left side.</dd>
                        <dt>Oven:</dt>
                        <dd>Inside door on the right side of the body.</dd>
                        <dt>Range:</dt>
                        <dd>Under the front edge of the cooktop.</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Bertazzoni Appliance Service and Repair</h1>
                        <p>We have been proudly serving Bertazzoni appliances for the last 20 years.  Bertazzoni manufactures cooking appliances for the home and professional.</p>

                        <p>Prior to a technician coming out to your home we would like to offer a few tips to possibly help repair the unit without the need for a service call.</p>

                        <ul>
                            <li>If the gas burners are not lighting properly or clicking after lit.  Your igniters may need cleaning, this can usually be done by cleaning the tip of the igniter with a fine file, or an emery board.  Follow that up with cleaning the igniter with a Q-tip dipped in rubbing alcohol.</li>
                            <li>On all other appliances that have an electronic controls, if the unit locks up or shows a fault code of any kind, turn the unit off at the circuit breaker and wait 5 minutes, then restore power to the unit.  Many times this will “reboot” the appliance and no more service is required.</li>
                        </ul>

                        <p>If after attempting these checks and the unit is still not working, or you need any assistance please call us at (206)365-9310 and we will schedule one of our service technicians to come out to your home.  We also offer many other appliance repair service for all your needs in the greater Puget Sound area.  Please see areas we service for a zip code list.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
