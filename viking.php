<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Viking Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Viking Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="viking.php"><img srcset="img/logos/viking.png, img/logos/viking@2x.png 2x" alt="Viking"></a>
                        </picture>
                    </div>
                    <p><a href="http://www.vikingrange.com">www.vikingrange.com</a></p>
                    <p><a href="tel:1-888-845-4641">1-888-845-4641</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>

                        <dt>Beverage Cooler:</dt>
                        <dd>Above light switch when door is open.</dd>
                        <dt>BBQ Carts:</dt>
                        <dd>Inside on left.</dd>
                        <dt>BBQ Grille:</dt>
                        <dd>Remove drip tray left front.</dd>
                        <dt>BBQ Side Burner:</dt>
                        <dd>Left side burner box.</dd>
                        <dt>Compactor:</dt>
                        <dd>Open drawer left side of frame.</dd>
                        <dt>Cooktop:</dt>
                        <dd>Right side under unit.</dd>
                        <dt>Cooktop Sealed Burner:</dt>
                        <dd>Under burner box.</dd>
                        <dt>Cooktop Open Burner:</dt>
                        <dd>Left side rear under top.</dd>
                        <dt>Dishwasher:</dt>
                        <dd>Right side of inner door panel.</dd>
                        <dt>Disposer:</dt>
                        <dd>Below reset button.</dd>
                        <dt>Dual Fuel Range:</dt>
                        <dd>Behind access panel at bottom.</dd>
                        <dt>Downdraft:</dt>                            
                        <dd>On motor cover.</dd>
                        <dt>Electric Range:</dt>
                        <dd>Under trim on left side.</dd>
                        <dt>Gas Range:</dt>
                        <dd>Open burner, side well left of burner.</dd>
                        <dt>Hood (exterior):</dt>
                        <dd>Vent side.</dd>
                        <dt>Hood (interior):</dt>
                        <dd>Front of exhaust motor.</dd>
                        <dt>Ice Maker:</dt>
                        <dd>Open bin door, left side of frame.</dd>
                        <dt>Microwave:</dt>
                        <dd>Top center front.</dd>
                        <dt>Oven (Double):</dt>
                        <dd>Left side front frame lower oven.</dd>
                        <dt>Oven (Single):</dt>
                        <dd>Left side of door frame.</dd>
                        <dt>Range (dual fuel):</dt>
                        <dd>On side wall at left rear burner.</dd>
                        <dt>Range (electric):</dt>
                        <dd>Under trim above door opening.</dd>
                        <dt>Refrigerator:</dt>
                        <dd>Behind access panel center crisper.</dd>
                        <dt>Refrigerator Bottom Mount:</dt>
                        <dd>Side wall of produce drawer or up on ceiling right front.</dd>
                        <dt>Refrigerator Side by Side:</dt>
                        <dd>Divider wall of freezer.</dd>
                        <dt>Refrigerator (small):</dt>
                        <dd>Inside top right.</dd>
                        <dt>Refrigerator Under Counter:</dt>
                        <dd>Lower right side front, or lower left front frame.</dd>
                        <dt>Warming Drawer:</dt>
                        <dd>Upper left side.</dd>
                        <dt>Warming Drawer Desiger Series:</dt>
                        <dd>Pull drawer to stop, lift out on left side rear.</dd>
                        <dt>Wine Cooler:</dt>
                        <dd>Lower right side front.</dd>
                        <dt>Wine Cooler (Tall):</dt>
                        <dd>Right side liner.</dd>
                        <dt>Wok Cooker:</dt>
                        <dd>Left side burner box (lift grates and grate supports).</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Viking Appliance Service and Repair</h1>
                        <p>We having been proudly servicing Viking appliance for over 25 since Viking’s introduction to the market.  Viking manufactures ranges, wall ovens, cooktops, microwaves, BBQ’s, dishwashers, trash compactors, ventilation products and refrigeration.  We do not currently service the refrigerators.</p>

                        <p>Prior to a technician coming out to your home we would like to offer a few tips to possibly help repair the unit without the need for a service call.</p>

                        <ul>
                            <li>If your dishwasher is not draining, remove the filter in the bottom of unit and ensure the filter is clean, if your dishwasher is draining to an air gap, remove the cap on the air gap and inspect it for clogs.</li>
                            <li>If the dishwasher is not filling with water, check to make sure the water supply valve (usually under the kitchen sink) is turned on.</li>
                            <li>If the gas burners are not lighting properly or clicking after lit.  Your igniters may need cleaning, this can usually be done by cleaning the tip of the igniter with a fine file, or an emery board.  Follow that up with cleaning the igniter with a Q-tip dipped in rubbing alcohol.</li>
                            <li>On barbeques the burner tubes can become clogged with food debris and will require occasional cleaning.   This can be accomplished by using a small piece of wire or paperclip and going around to all the holes in the burner and cleaning them out.</li>
                            <li>On all other appliances that have an electronic controls, if the unit locks up or shows a fault code of any kind, turn the unit off at the circuit breaker and wait 5 minutes, then restore power to the unit.  Many times this will “reboot” the appliance and no more service is required.</li>
                        </ul>

                        <p>If after attempting these checks and the unit is still not working, or you need any assistance please call us at (206)365-9310 and we will schedule one of our service technicians to come out to your home.  We also offer many other appliance repair service for all your needs in the greater Puget Sound area.  Please see areas we service for a zip code list.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
