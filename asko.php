<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Asko Appliance Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Asko Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="asko.php"><img srcset="img/logos/asko.png, img/logos/asko@2x.png 2x" alt="Asko"></a>
                        </picture>
                    </div>
                    <p><a href="http://www.asko.com">www.asko.com</a></p>
                    <p><a href="tel:1-800-898-1879">1-800-898-1879</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>
                        <dt>Dishwasher:</dt>
                        <dd>Right side inner door panel.</dd>
                        <dt>Dryer:</dt>
                        <dd>Upper corner of the inner door panel.</dd>
                        <dt>Washer:</dt>
                        <dd>Inner door panel.</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Asko Appliance Service and Repair</h1>
                        <p>We have been proudly serving Asko appliances for the last 20 years.  Asko manufactures washer and dryer laundry products.</p>

                        <p>Prior to a technician coming out to your home we would like to offer a few tips to possibly help repair the unit without the need for a service call.</p>

                        <ul>
                            <li>If the washing machine is not draining it is possible that a foreign object has gotten stuck in the drain pump impellor.  On most Asko washing machines you will find a small door in the kick plate area, after opening the door you should see a small black hose that will enable you to drain the remaining water out, (a cake pan works well to catch the water) after the water is removed you unscrew the pump insert by turning it counterclockwise and pulling outwards.  At this point you should be able to remove the object from the drain pump.</li>
                            <li>If you are experiencing longer than normal dry times it is most likely an air flow issue.  The two most common culprits are the venting and lint screen.  You should be able to hold the lint screen up to a light and see right through it.  If you can’t see through the lint screen needs to be cleaned with warm water and a scrub brush.  The venting from the dryer to the outside of the home should be free of obstructions and should be 4 inch metal venting.  Plastic venting is not recommended on any dryer and poses a fire hazard. </li>
                        </ul>

                        <p>If after attempting these checks and the unit is still not working, or you need any assistance please call us at (206)365-9310 and we will schedule one of our service technicians to come out to your home.  We also offer many other appliance repair service for all your needs in the greater Puget Sound area.  Please see areas we service for a zip code list.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
