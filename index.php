<?php require_once('head.php'); ?>
    <title>Appliance Service Station Inc. - High End Appliance Repair</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <div class="content col-xs-12 col-md-10">
                    <section>
                        <h1 class="red-gradient">Appliance Service Station Inc - Premium Appliance Repair</h1>
                        <p>Your appliances are a vital part of your household and you need them to be fully functional. What do you do when your refrigerator breaks down? What about your washing machine? These situations aren’t just inconvenient; they’re disastrous.</p>
                        <p>Appliance Service Station is here to help. With over 65 years of experience servicing a variety of appliances, we’re here to help your household run smoothly again.</p>
                        <p>Appliance Service Station Inc also serves Bellevue, Mercer Island, Kirkland, Issaquah, and Surrounding Areas</p>
                    </section>
                    <section>
                        <h2>Prompt, Professional Service</h2>
                        <p>Appliance Service Station is locally-owned, and we understand the importance of prompt, professional service. We have won several awards as a Seattle, WA, appliance repair service company.</p>
                        <p>We received the Bosch Servicer of the Year in 2013 for the Northwest region. We have also won the Angie’s List Super Servicer Award for seven years in a row.</p>
                        <p>All our technicians are factory trained and fully licensed as specialty electricians. We work closely with our manufacturers to ensure we are always up to date on the latest advances and designs. This means we repair your appliances promptly and correctly.</p>
                        <p>We stand by all our work. We guarantee our repairs for one year after the repair date.</p>
                    </section>
                    <section>
                        <h2>Top of the line products</h2>
                        <p>Our goal is to serve all your appliance needs in a single visit, saving you time and money. We service a variety of top-of-the-line brands in the Seattle area, including:</p>
                        <div class="row service-partners">
                            <div class="col-xs-12 col-sm-7 col-sm-offset-1 col-md-6 col-md-offset-1 col-lg-5 clearfix">
                                <h4>Authorized Service Partner for:</h4>
                                <ul>
                                    <li>Bosch</li>
                                    <li>Dacor</li>
                                    <li>Miele</li>
                                    <li>DCS</li>
                                    <li>Fisher &amp; Paykel</li>
                                    <li>Gaggenau</li>
                                </ul>
                                <ul>
                                    <li>Thermador</li>
                                    <li>Zephyr</li>
                                    <li>U-Line</li>
                                    <li>Bertazzoni</li>
                                    <li>BlueStar</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 clearfix">
                                <h4>Also Servicing:</h4>
                                <ul>
                                    <li>Sub-Zero</li>
                                    <li>Wolf</li>
                                    <li>Asko</li>
                                    <li>Viking</li>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <section>
                        <h2>Let us help</h2>
                        <p>No matter what the brand or problem is, let Appliance Service Station be your first choice for appliance repair service in Seattle, WA. Give us a call at 1-866-797-0670 to set up an appointment with one of our expert technicians.</p>
                        <figure class="align-center">
                            <div class="image-holder tex">
                                <picture>
                                    <img srcset="img/service-trucks.jpg, img/service-trucks@2x.jpg 2x" alt="Appliance Service Station service vehicles from the 1940s">
                                </picture>
                                <figcaption>Proudly serving the Puget Sound area since 1947</figcaption>
                            </div>
                        </figure>
                    </section>
                </div>
                <aside class="col-xs-12 col-sm-10 col-sm-push-1 col-md-2 col-md-push-0 align-center no-left-pad">
                    <?php include_once('brands.php'); ?>
                </aside>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
