<section id="survey-form">
	<h1 class="red-gradient">Customer Service Survey</h1>
	<p>Please take a few minutes to fill out this simple 10 question survey to let us know how we're doing.</p>

	[[!FormIt?
	&hooks=`email,redirect`
	&emailTpl=`surveyEmailTpl`
	&emailTo=`service@applianceservicestation.com`
	&emailSubject=`Customer Service Survey`
    &store=`1`
	&redirectTo=`25`
	]]

	<form action="" id="[[~[[*id]]]]" method="post">
		<fieldset class="required">
			<legend>Survey questions</legend>

			<div class="">
				<div class="form-field">
					<h3>What is your name?</h3>
					<input type="text" name="customerName" id="customerName" value="">
				</div>
			</div>
			<div class="row">
				<div class="form-field col-xs-12 col-md-6">
					<h3>1. Overall, how satisfied or dissatisfied are you with your service from Appliance Service Station?</h3>
					<ul>
						<li>
							<input type="radio" name="q1" id="1a" value="Very satisfied">
							<label for="1a">Very satisfied</label>
						</li>
						<li>
							<input type="radio" name="q1" id="1b" value="Satisfied">
							<label for="1b">Satisfied</label>
						</li>
						<li>
							<input type="radio" name="q1" id="1c" value="Neither satisfied nor dissatisfied">
							<label for="1c">Neither satisfied nor dissatisfied</label>
						</li>
						<li>
							<input type="radio" name="q1" id="1d" value="Dissatisfied">
							<label for="1d">Dissatisfied</label>
						</li>
						<li>
							<input type="radio" name="q1" id="1e" value="Very dissatisfied">
							<label for="1e">Very dissatisfied</label>
						</li>
					</ul>
				</div>
				<div class="form-field col-xs-12 col-md-6">
					<h3>2. Which of the following words would you use to describe our services? Select all that apply.</h3>

					<ul>
						<li>
							<input type="checkbox" name="Reliable" id="2a" value="1">
							<label for="2a">Reliable</label>
						</li>
						<li>
							<input type="checkbox" name="High_quality" id="2b" value="1">
							<label for="2b">High Quality</label>
						</li>
						<li>
							<input type="checkbox" name="Useful" id="2c" value="1">
							<label for="2c">Useful</label>
						</li>
						<li>
							<input type="checkbox" name="Good_value" id="2d" value="1">
							<label for="2d">Good value</label>
						</li>
						<li>
							<input type="checkbox" name="Overpriced" id="2e" value="1">
							<label for="2e">Overpriced</label>
						</li>
						<li>
							<input type="checkbox" name="Ineffective" id="2f" value="1">
							<label for="2f">Ineffective</label>
						</li>
						<li>
							<input type="checkbox" name="Unreliable" id="2g" value="1">
							<label for="2g">Unreliable</label>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="form-field col-xs-12 col-md-6">
					<h3>3. How well do our services meet your needs?</h3>

					<ul>
						<li>
							<input type="radio" name="q3" id="3a" value="Extremely well">
							<label for="3a">Extremely well</label>
						</li>
						<li>
							<input type="radio" name="q3" id="3b" value="Very well">
							<label for="3b">Very well</label>
						</li>
						<li>
							<input type="radio" name="q3" id="3c" value="Somewhat well">
							<label for="3c">Somewhat well</label>
						</li>
						<li>
							<input type="radio" name="q3" id="3d" value="Not so well">
							<label for="3d">Not so well</label>
						</li>
						<li>
							<input type="radio" name="q3" id="3e" value="Not well at all">
							<label for="3e">Not well at all</label>
						</li>
					</ul>
				</div>
				<div class="form-field col-xs-12 col-md-6">
					<h3>4. How would you rate the quality of our services?</h3>

					<ul>
						<li>
							<input type="radio" name="q4" id="4a" value="Very high quality">
							<label for="4a">Very high quality</label>
						</li>
						<li>
							<input type="radio" name="q4" id="4b" value="High quality">
							<label for="4b">High quality</label>
						</li>
						<li>
							<input type="radio" name="q4" id="4c" value="Neither high nor low quality">
							<label for="4c">Neither high nor low quality</label>
						</li>
						<li>
							<input type="radio" name="q4" id="4d" value="Low quality">
							<label for="4d">Low quality</label>
						</li>
						<li>
							<input type="radio" name="q4" id="4e" value="Very low quality">
							<label for="4e">Very low quality</label>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="form-field col-xs-12 col-md-6">
					<h3>5. How would you rate the value for money of our services?</h3>

					<ul>
						<li>
							<input type="radio" name="q5" id="5a" value="Excellent">
							<label for="5a">Excellent</label>
						</li>
						<li>
							<input type="radio" name="q5" id="5b" value="Above average">
							<label for="5b">Above average</label>
						</li>
						<li>
							<input type="radio" name="q5" id="5c" value="Average">
							<label for="5c">Average</label>
						</li>
						<li>
							<input type="radio" name="q5" id="5d" value="Below average">
							<label for="5d">Below average</label>
						</li>
						<li>
							<input type="radio" name="q5" id="5e" value="Poor">
							<label for="5e">Poor</label>
						</li>
					</ul>
				</div>
				<div class="form-field col-xs-12 col-md-6">
					<h3>6. How responsive have we been to your questions or concerns?</h3>

					<ul>
						<li>
							<input type="radio" name="q6" id="6a" value="Extremely responsive">
							<label for="6a">Extremely responsive</label>
						</li>
						<li>
							<input type="radio" name="q6" id="6b" value="Very responsive">
							<label for="6b">Very responsive</label>
						</li>
						<li>
							<input type="radio" name="q6" id="6c" value="Moderately responsive">
							<label for="6c">Moderately responsive</label>
						</li>
						<li>
							<input type="radio" name="q6" id="6d" value="Not so responsive">
							<label for="6d">Not so responsive</label>
						</li>
						<li>
							<input type="radio" name="q6" id="6e" value="Not responsive at all">
							<label for="6e">Not responsive at all</label>
						</li>
						<li>
							<input type="radio" name="q6" id="6f" value="Not applicable">
							<label for="6f">Not applicable</label>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="form-field col-xs-12 col-md-6">
					<h3>7. How long have you been a customer of our company?</h3>

					<ul>
						<li>
							<input type="radio" name="q7" id="7a" value="This is my first time using you">
							<label for="7a">This is my first time using you</label>
						</li>
						<li>
							<input type="radio" name="q7" id="7b" value="Less than 6 months">
							<label for="7b">Less than 6 months</label>
						</li>
						<li>
							<input type="radio" name="q7" id="7c" value="Six months to a year">
							<label for="7c">Six months to a year</label>
						</li>
						<li>
							<input type="radio" name="q7" id="7d" value="1 - 2 years">
							<label for="7d">1 - 2 years</label>
						</li>
						<li>
							<input type="radio" name="q7" id="7e" value="3 or more years">
							<label for="7e">3 or more years</label>
						</li>
					</ul>
				</div>
				<div class="form-field col-xs-12 col-md-6">
					<h3>8. How likely are you to purchase any of our services again?</h3>

					<ul>
						<li>
							<input type="radio" name="q8" id="8a" value="Extremely likely">
							<label for="8a">Extremely likely</label>
						</li>
						<li>
							<input type="radio" name="q8" id="8b" value="Very likely">
							<label for="8b">Very likely</label>
						</li>
						<li>
							<input type="radio" name="q8" id="8c" value="Somewhat likely">
							<label for="8c">Somewhat likely</label>
						</li>
						<li>
							<input type="radio" name="q8" id="8d" value="Not so likely">
							<label for="8d">Not so likely</label>
						</li>
						<li>
							<input type="radio" name="q8" id="8e" value="Not likely at all">
							<label for="8e">Not likely at all</label>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="form-field col-xs-12">
					<h3>9. How likely are you to refer us to a friend?</h3>
					<div class="inline-question ten-question">
						<div>
							<input type="radio" name="q9" id="9a" value="1">
							<label for="9a">1</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9b" value="2">
							<label for="9b">2</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9c" value="3">
							<label for="9c">3</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9d" value="4">
							<label for="9d">4</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9e" value="5">
							<label for="9e">5</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9f" value="6">
							<label for="9f">6</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9g" value="7">
							<label for="9g">7</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9h" value="8">
							<label for="9h">8</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9i" value="9">
							<label for="9i">9</label>
						</div>
						<div>
							<input type="radio" name="q9" id="9j" value="10">
							<label for="9j">10</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-field col-xs-12">
					<h3>10. Was anybody from Appliance Service Station particular helpful?</h3>
					<select name="q10" id="10a">
                        <option value="" selected="selected"></option>
                        <optgroup label="Office Staff">
                            <option value="Deb">Deb</option>
                            <option value="Holly">Holly</option>
                            <option value="Carol">Carol</option>
                            <option value="Carl">Carl</option>
                        </optgroup>
                        <optgroup label="Technicians">
                            <option value="Scott">Scott</option>
                            <option value="Glenn">Glenn</option>
                            <option value="Tony">Tony</option>
                            <option value="Dustin">Dustin</option>
                            <option value="Kevin">Kevin</option>
                            <option value="Carl">Carl</option>
                        </optgroup>
                    </select>
				</div>
			</div>
			<div class="row">
				<div class="form-field col-xs-12">
					<h3 class="block">11. Do you have any comments, questions, or concerns?</h3>
					<textarea name="q11" id="11a" cols="30" rows="5" value="[[!+fi.message]]"></textarea>
				</div>
			</div>
		</fieldset>
		<div class="col-xs-12 col-md-11 align-center">
			<input id="survey-form-button" class="button submit tex" value="Send" type="submit" />
			<img src="img/ajax-loader.gif" alt="loading..." class="ajax-loader" style="display:none;">
		</div>
	</form>
</section>
