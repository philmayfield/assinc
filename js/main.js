$(document).ready(function(){
	$('nav ul').droptab({
		breakWidth : 768,
		targetWidth : $('body'),
		openMsg : 'Menu'
	});

	var s, 
		sliderVis = $('#slider').hasClass('visuallyhidden');

	sliderVis ? s = true : s = initSlider();

	$(window).on('resize',function(){
		if(!sliderVis){
			s = initSlider(s);
		}
	});

	if($('#service-form').length){
		var email = $('#email'),
			phone = $('#phone');
			fields = $('#service-form .required').find('input, textarea');

		fields.blur(function(){
			$(this).addClass('touched');
		});

		email.blur(function(){
			if($(this).is(':valid')){
				phone.removeAttr('required');
			} else {
				phone.attr('required',true);
			}
		});

		phone.blur(function(){
			if($(this).is(':valid')){
				email.removeAttr('required');
			} else {
				email.attr('required',true);
			}
		});
                $('#service-form form').on('submit',function(){
                    $('#service-form-button').hide().next().show();
                });
	}

	if($('#survey-form-button').length){
		$('#survey-form form').on('submit',function(){
        	$('#survey-form-button').hide().next().show();
        });
	}

	if($('#zip-check').length){
		$('#zip-check').on('submit',function(e){
			e.preventDefault();
			var zip = $('#zc-zip').val(),
				msg = $('#zc-msg');
			if($.inArray(zip, serviceAreaArray) >= 0){
				// in array
				msg.addClass('green').html('Yes - you are in our service area!').removeClass('red hidden');
			} else {
				// not in array
				msg.addClass('red').html('Sorry - you are not in our current service area').removeClass('green hidden');
			}
		});
	}

	if ($('#rec-val').length) {
		var recVal = parseInt($('#rec-val').text());
		if (recVal > 5) {
			$('#rec-pos').removeClass('hidden');
		}
	}

});

function initSlider(s) {
	if(s == true){
		return true;
	} else {
		if(Modernizr.mq('only all and (min-width: 992px)')){
			var slider = $('#slider'),
				activeItem,
				nextItem;

			// prevent FOUC on slides
			setTimeout(function(){
				slider.find('.slide').removeClass('visuallyhidden');
				slider.find('.slide:first-of-type').toggleClass('on off');
			}, 800);

			var sliderTimer = setInterval(function(){
			  
			    activeItem = slider.find('.on');

			    activeItem.is('.slide:last-child') ? nextItem = activeItem.siblings('.slide:first-child') : nextItem = activeItem.next();

			    activeItem.toggleClass('on off');

			    setTimeout(function(){
			    	nextItem.toggleClass('on off');
			    }, 300);

			}, 4000);

			return true;
		}
	}
}

var serviceAreaArray = [
	"98001",
	"98002",
	"98003",
	"98004",
	"98005",
	"98006",
	"98007",
	"98008",
	"98011",
	"98020",
	"98021",
	"98023",
	"98026",
	"98027",
	"98028",
	"98029",
	"98031",
	"98032",
	"98033",
	"98034",
	"98035",
	"98036",
	"98037",
	"98038",
	"98039",
	"98040",
	"98042",
	"98043",
	"98052",
	"98053",
	"98054",
	"98055",
	"98056",
	"98058",
	"98059",
	"98072",
	"98074",
	"98075",
	"98077",
	"98092",
	"98100",
	"98101",
	"98102",
	"98103",
	"98104",
	"98105",
	"98106",
	"98107",
	"98108",
	"98109",
	"98111",
	"98112",
	"98113",
	"98114",
	"98115",
	"98116",
	"98117",
	"98118",
	"98119",
	"98120",
	"98121",
	"98122",
	"98123",
	"98124",
	"98125",
	"98126",
	"98127",
	"98128",
	"98129",
	"98130",
	"98131",
	"98132",
	"98133",
	"98134",
	"98135",
	"98136",
	"98137",
	"98138",
	"98139",
	"98140",
	"98141",
	"98142",
	"98143",
	"98144",
	"98145",
	"98146",
	"98147",
	"98148",
	"98149",
	"98150",
	"98151",
	"98152",
	"98153",
	"98154",
	"98155",
	"98156",
	"98157",
	"98158",
	"98159",
	"98160",
	"98161",
	"98162",
	"98163",
	"98164",
	"98165",
	"98166",
	"98167",
	"98168",
	"98169",
	"98170",
	"98171",
	"98172",
	"98173",
	"98174",
	"98175",
	"98176",
	"98177",
	"98178",
	"98179",
	"98180",
	"98181",
	"98182",
	"98183",
	"98184",
	"98185",
	"98186",
	"98187",
	"98188",
	"98189",
	"98190",
	"98191",
	"98192",
	"98193",
	"98194",
	"98195",
	"98196",
	"98197",
	"98198",
	"98199",
	"98201",
	"98203",
	"98204",
	"98208",
	"98275"
];