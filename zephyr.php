<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - Zephyr Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">Zephyr Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="zephyr.php"><img srcset="img/logos/zephyr.png, img/logos/zephyr@2x.png 2x" alt="Zephyr"></a>
                        </picture>
                    </div>
                    <p class="approved"><span class="icon">Checkmark</span> Appliance Service Station is a U-Line certified repair provider.</p>
                    <p><a href="http://zephyronline.com">zephyronline.com</a></p>
                    <p><a href="tel:1-888-880-8368">1-888-880-8368</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>
                        <dt>Hoods:</dt>
                        <dd>Behind filters, must call Zephyr first.</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">Zephyr Range Hood Service and Repair</h1>
                        <p>We are proud to service Zephyr products.  We are a factory certified repair center.  Zephyr manufactures top of the line range hoods.  Our technicians are factory trained on all these appliances.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
