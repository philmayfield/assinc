<?php 
    require_once('head.php'); 
    $headerSize = 's';
?>
    <title>Appliance Service Station Inc. - U-Line Service and Repair - Seattle, WA</title>
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include_once('header.php'); ?>
        <main>
            <div class="container">
                <aside class="col-xs-12 col-md-4 col-md-push-8">
                    <h2 class="align-center">U-Line Info</h2>
                    <div class="align-center">
                        <picture class="center">
                            <a href="u-line.php"><img srcset="img/logos/u-line.png, img/logos/u-line@2x.png 2x" alt="U-Line"></a>
                        </picture>
                    </div>
                    <p class="approved"><span class="icon">Checkmark</span> Appliance Service Station is a U-Line certified repair provider.</p>
                    <p><a href="http://www.u-line.com/?___store=us">www.u-line.com</a></p>
                    <p><a href="tel:1-800-779-2547">1-800-779-2547</a></p>
                    <h2>Model &amp; Serial Number Location</h2>
                    <dl>
                        <dt>Ice Maker:</dt>
                        <dd>Back or inside on side wall.</dd>
                        <dt>Refrigerator:</dt>
                        <dd>Inside on side wall.</dd>
                    </dl>
                </aside>
                <div class="content col-xs-12 col-md-8 col-md-pull-4">
                    <section>
                        <h1 class="red-gradient">U-Line Appliance Service and Repair</h1>
                        <p>We are proud to service U-Line products.  We are a factory certified repair center.  U-Line manufactures under counter refrigerators, wine coolers, ice makers and freezers.  Our technicians are factory trained on all these appliances.</p>
                    </section>
                </div>
            </div>
        </main>
        
        <?php 
        include_once('footer.php'); 
        include_once('slider.php');
        include_once('scripts.php');
        ?>
    </body>
</html>
