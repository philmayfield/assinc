<header class="dark-blue-gradient tex">
    <div class="stripe-bg">
        <div class="container<?php if(isset($headerSize) && $headerSize == 's'){echo ' small';} ?>">
            <div class="logo grey-white-gradient">
                <a href="index.php"><img src="img/logo.svg" alt="Appliance Service Station Inc. - Since 1947"></a>
            </div>
            <span class="info">Mon-Fri 8am to 5pm <strong><a href="tel:206-365-9310">1-206-365-9310</a></strong></span>
            <nav>
                <ul>
                    <li class="home <?php if(strpos($_SERVER['PHP_SELF'],'index') > 0){echo ' active';}?>"><a href="index.php">Home</a></li><li<?php if(strpos($_SERVER['PHP_SELF'],'brands_we_service') > 0){echo ' class="active"';}?>>
                    <a href="brands_we_service.php">Brands We Service</a></li><li<?php if(strpos($_SERVER['PHP_SELF'],'request_service') > 0){echo ' class="active"';}?>>
                    <a href="request_service.php">Request Service</a></li><li<?php if(strpos($_SERVER['PHP_SELF'],'signature_polish') > 0){echo ' class="active"';}?>>
                    <a href="signature_polish.php">Signature Polish</a></li><li<?php if(strpos($_SERVER['PHP_SELF'],'tips_and_hints') > 0){echo ' class="active"';}?>>
                    <a href="tips_and_hints.php">Tips &amp; Hints</a></li><li<?php if(strpos($_SERVER['PHP_SELF'],'about_us') > 0){echo ' class="active"';}?>>
                    <a href="about_us.php">About Us</a></li>
                </ul>
            </nav>
            <div class="serving fs1 <?php if(isset($headerSize) && $headerSize == 's'){echo ' visuallyhidden';} ?>"><span class="no-wrap">Proudly serving the Puget Sound</span> <span class="no-wrap">area since 1947</span></div>
        </div>
    </div>
</header>
